package com.manko.task12;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SyncClassWithLock {

    private final static Lock lock = new ReentrantLock();
    private static Logger logger = LogManager.getLogger(SyncClassWithLock.class);

    public void methodOne() {
        lock.lock();
        try {
            for (int i = 0; i < 5; i++) {
                logger.info("MethodOne number = " + i + " " + Thread.currentThread().getName());
            }
        } finally {
            lock.unlock();
        }
        logger.info(Thread.currentThread().getName() + " finished.");
    }

    public void methodTwo() {
        lock.lock();
        try {
            for (int i = 10; i < 15; i++) {
                logger.info("MethodTwo number = " + i + " " + Thread.currentThread().getName());
            }
        } finally {
            lock.unlock();
        }
        logger.info(Thread.currentThread().getName() + " finished.");
    }

    public void methodThree() {
        lock.lock();
        try {
            for (int i = 20; i < 25; i++) {
                logger.info("MethodThree number = " + i + " " + Thread.currentThread().getName());
            }
        } finally {
            lock.unlock();
        }
        logger.info(Thread.currentThread().getName() + " finished.");
    }
}

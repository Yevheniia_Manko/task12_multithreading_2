package com.manko.task12;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class TaskWithQueue {

    private static Logger logger = LogManager.getLogger(TaskWithQueue.class);

    public static void main(String[] args) {
        BlockingQueue<String> queue = new PriorityBlockingQueue<>();
        Runnable taskTake = new Runnable() {
            @Override
            public void run() {
                for (int i = 65; i < 91; i++) {
                    try {
                        logger.info(Thread.currentThread().getName());
                        logger.info(queue.take());
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                logger.info(Thread.currentThread().getName() + " finished.");
            }
        };
        Runnable taskPut = new Runnable() {
            @Override
            public void run() {
                for (int i = 65; i < 91; i++) {
                    try {
                        logger.info(Thread.currentThread().getName());
                        queue.put(String.valueOf((char)i));
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                logger.info(Thread.currentThread().getName() + " finished.");
            }
        };
        Thread t1 = new Thread(taskTake, "Thread TaskTake");
        t1.start();
        Thread t2 = new Thread(taskPut, "Thread TaskPut");
        t2.start();
    }
}

package com.manko.task12;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AsyncClassWithLock {

    private final static Lock lock1 = new ReentrantLock();
    private final static Lock lock2 = new ReentrantLock();
    private final static Lock lock3 = new ReentrantLock();
    private static Logger logger = LogManager.getLogger(AsyncClassWithLock.class);

    public void methodOne() {
        lock1.lock();
        try {
            for (int i = 0; i < 5; i++) {
                logger.info("MethodOne number = " + i + " " + Thread.currentThread().getName());
            }
        } finally {
            lock1.unlock();
        }
        logger.info(Thread.currentThread().getName() + " finished.");
    }

    public void methodTwo() {
        lock2.lock();
        try {
            for (int i = 10; i < 15; i++) {
                logger.info("MethodTwo number = " + i + " " + Thread.currentThread().getName());
            }
        } finally {
            lock2.unlock();
        }
        logger.info(Thread.currentThread().getName() + " finished.");
    }

    public void methodThree() {
        lock3.lock();
        try {
            for (int i = 20; i < 25; i++) {
                logger.info("MethodThree number = " + i + " " + Thread.currentThread().getName());
            }
        } finally {
            lock3.unlock();
        }
        logger.info(Thread.currentThread().getName() + " finished.");
    }
}

package com.manko.task12;

public class SomeClassWithLockTest {

    public static void main(String[] args) {
        // uncomment for testing asynchronized methods
//        AsyncClassWithLock someClass = new AsyncClassWithLock();
        // uncomment for testing synchronized methods
        SyncClassWithLock someClass = new SyncClassWithLock();

        Thread t1 = new Thread(someClass::methodOne);
        Thread t2 = new Thread(someClass::methodTwo);
        Thread t3 = new Thread(someClass::methodThree);
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
